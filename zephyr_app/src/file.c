// Copyright (c) 2022-2023, Arm Limited.
// SPDX-License-Identifier: Apache-2.0

#include <stdio.h>

FILE *fopen(const char *filename, const char *mode)
{
  return NULL;
}

int fclose(FILE *stream)
{
  return 0;
}

size_t fread(void *ptr, size_t size, size_t count, FILE *stream)
{
  return 0;
}
