# Based on: https://github.com/autowarefoundation/autoware_common/blob/ba4d3cc3f729ebedc16aba7d745bcc54fd935e61/autoware_cmake/autoware_cmake-extras.cmake
# In open-source project: autoware_common
# Original file: Copyright (c) 2022, The Autoware Contributors
# Modifications: Copyright (c) 2023, Arm Limited.
# SPDX-License-Identifier: Apache-2.0

include("${autoware_cmake_DIR}/autoware_package.cmake")
