// Based on: https://gitlab.com/autowarefoundation/autoware.auto/AutowareAuto/-/blob/e3e26be1ab4b822996df60f261d031d7924f20ac/src/common/autoware_auto_geometry/include/geometry/bounding_box_2d.hpp
// In open-source project: Autoware.Auto
// Original file: Copyright (c) 2017-2020, the Autoware Foundation
// Modifications: Copyright (c) 2023, Arm Limited.
// SPDX-License-Identifier: Apache-2.0
//
// Co-developed by Tier IV, Inc. and Apex.AI, Inc.
/// \file
/// \brief Main header for user-facing bounding box algorithms: functions and types
#ifndef GEOMETRY__BOUNDING_BOX_2D_HPP_
#define GEOMETRY__BOUNDING_BOX_2D_HPP_

#include <geometry/bounding_box/rotating_calipers.hpp>
#include <geometry/bounding_box/eigenbox_2d.hpp>
#include <geometry/bounding_box/lfit.hpp>

namespace autoware
{
namespace common
{
namespace geometry
{
}  // namespace geometry
}  // namespace common
}  // namespace autoware
#endif  // GEOMETRY__BOUNDING_BOX_2D_HPP_
